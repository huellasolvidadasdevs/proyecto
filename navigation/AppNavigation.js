
import { createAppContainer } from "react-navigation" ;
import { createDrawerNavigator } from 'react-navigation-drawer'; 
import PrincipalDogs from '../src/screens/DogsManager/PrincipalScreen'
import History from '../src/screens/DogsManager/HistoryDogs'


import { DrawerNavigator, StackNavigator } from 'react-navigation';
import { Navigation, Navigation_Admin_Request,Navigation_Login } from "../App";

import Requestlist from "../src/screens/Admin-Adoption/Request-list/Requestlist";


const AppNavigation= createDrawerNavigator({

    'Principal Perros' :{ 
        screen: Navigation
    },
    'Historia':{
        screen: History
    },
    'Lista de Solicitudes' :{ 
        screen: Navigation_Admin_Request
    },
    'Iniciar Sesión' :{ 
        screen: Navigation_Login
    }


},
{
    initialRouteName: 'Principal Perros',
    drawerBackgroundColor: '#fff'
  })
 
export default createAppContainer(AppNavigation)