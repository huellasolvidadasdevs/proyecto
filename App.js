import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import AppNavigation from './navigation/AppNavigation.js';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import {DrawerItems,DrawerActions} from 'react-navigation-drawer';
import { Icon, SearchBar, Button } from 'react-native-elements';
import { render } from 'react-dom';

//Screens
import Requestlist from "./src/screens/Admin-Adoption/Request-list/Requestlist";
import RequestDetails from "./src/screens/Admin-Adoption/Request-Details/RequestDetails";
import PrincipalDogs from './src/screens/DogsManager/PrincipalScreen.js'
import MenuAdmin from './src/screens/DogsManager/DogsMenuAdmin.js'
import NewDogs from './src/screens/DogsManager/CreateDogs.js'
import { styles } from './src/screens/DogsManager/StylesPrincipal.js'
import { Reports } from './src/screens/DogsRescueAdmi/Reports.js'
import Login from './src/screens/UserManagement/Login'
import UserProfile from './src/screens/UserManagement/UserProfile'
import UserInfo from './src/screens/UserManagement/UserInfo'

const Stack = createStackNavigator();

export default function App(){
    return (
      <SafeAreaProvider><AppNavigation></AppNavigation></SafeAreaProvider>
        
      
    );
}


export function Navigation_Login(){
  return(
    <NavigationContainer>
    <Stack.Navigator initialRouteName="Login">

      <Stack.Screen 
        name="Login" 
        component={Login}
        options={{
          headerShown: false, // change this to `false`
        }}
        />

      <Stack.Screen 
        name="UserProfile" 
        component={UserProfile}
        options={{
          headerShown: false, // change this to `false`
        }}
        />

      <Stack.Screen 
        name="UserInfo" 
        component={UserInfo}
        options={{
          headerShown: false, // change this to `false`
        }}
        />

    </Stack.Navigator>
  </NavigationContainer>
  )

}


export function Navigation_Admin_Request(){
  return(
    <NavigationContainer>
    <Stack.Navigator initialRouteName="Requestlist">

      <Stack.Screen 
        name="Requestlist" 
        component={Requestlist}
        options={{ 
          title: "Lista de Solicitudes",
          headerStyle: {backgroundColor: '#A67F5D'}, }}
        />

      <Stack.Screen 
        name="RequestDetails" 
        component={RequestDetails}
        options={{ 
          title: "Detalles de la Solicitud",
          headerStyle: {backgroundColor: '#A67F5D'}, }}
        />

    </Stack.Navigator>
  </NavigationContainer>
  )

}

export function Navigation(){
   
  return(
    <NavigationContainer>
    <Stack.Navigator initialRouteName="Principal">

      <Stack.Screen 
        name="Principal" 
        component={PrincipalDogs} 
        options={{
          headerTitle: "Perritoss",
          headerTintColor: '#fff',
          headerStyle: {backgroundColor: '#A67F5D'},
          headerLeft: ()=>(
            <Button icon={<Icon name="bars" size={18} type="font-awesome" color="white">

            </Icon> }
            
            
            >
            </Button>
          ),
          headerRight: () => (
            <Button icon={ <Icon name="plus" size={18} type="font-awesome" color="white"/> }
              onPress={() => alert('This is a button!')}
              type="clear"
              containerStyle= {styles.buttonAdd}
            />
            ),}}
          />

        <Stack.Screen name="Menu" component={MenuAdmin} 
        options= {{headerTitle: "Menú",
          headerTintColor: '#fff',
          headerStyle: {backgroundColor: '#A67F5D'}}}/>
        <Stack.Screen name="Create" component={NewDogs} />
      </Stack.Navigator>
      </NavigationContainer>
  )
}

class Search extends React.Component{
  state = {
    search: '',
  };
  updateSearch = (search) => {
    this.setState({ search });
  };
  render(){
    const {search} = this.state;
  
    return(
      <SearchBar
            placeholder="Type Here..."
            onChangeText={this.updateSearch}
            value={search}
            lightTheme
          />
    );}
}

