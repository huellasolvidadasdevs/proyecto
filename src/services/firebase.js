import firebase from 'firebase'

import 'firebase/firestore'

var firebaseConfig = {
    apiKey: "AIzaSyACC7XuGTo9yWLQCXoQPIxG0rJUYi1WysM",
    authDomain: "huellasolvidadas-dev.firebaseapp.com",
    projectId: "huellasolvidadas-dev",
    storageBucket: "huellasolvidadas-dev.appspot.com",
    messagingSenderId: "915812111210",
    appId: "1:915812111210:web:0481a9bd20987902d8cb13",
    measurementId: "G-12VJGMYWJ0"
};

firebase.initializeApp(firebaseConfig);
const db = firebase.firestore()

export default {
    firebase,
    db,
}