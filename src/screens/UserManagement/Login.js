import React from 'react';
import  firebase from '../../services/firebase';
import * as Google from 'expo-google-app-auth';
import * as Facebook from 'expo-facebook';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import { Button} from 'react-native-elements';

export default function Login ({navigation}){
  
  const signInGoogle =  async () => {
    var email;
    var site = "google";
    console.log("LoginScreen.js 6 | loggin in");
    try {
      const { type, user } = await Google.logInAsync({
        androidClientId: `954746852754-srrb16ht99dgn6lvfo05kgd4sfkbma5q.apps.googleusercontent.com`,
      });

      if (type === "success") {
        // Then you can use the Google REST API
        singUp(user,"google");
        email = user.email;
      }
    } catch (error) {
      console.log("LoginScreen.js 19 | error with login", error);
    }
    navigation.navigate("UserProfile", {email, site});
  };

  const signInFacebook =  async () => {
    var id;
    var site = "facebook" ;
    try {
      await Facebook.initializeAsync({
        appId: '1119552881825108',
      });
      const {
        type,
        token,
        expirationDate,
        permissions,
        declinedPermissions,
      } = await Facebook.logInWithReadPermissionsAsync({
        permissions: ['public_profile','email'],
      });
      if (type === 'success') {
        // Get the user's name using Facebook's Graph API
        const response = await fetch(`https://graph.facebook.com/me?access_token=${token}`);

        id = (await response.json()).id;
        var api = 'https://graph.facebook.com/v2.8/' + id +
                '?fields=name,email,picture.type(large)&access_token=' + token;

        await fetch(api)
        .then((response) => response.json())
        .then( (responseData) => {
            singUp(responseData,"facebook");
            console.log(responseData)
        })
        .done()

      } else {
        // type === 'cancel'
      }
    } catch ({ message }) {
      alert(`Facebook Login Error: ${message}`);
    }

    navigation.navigate("UserProfile", {id,site});  
  }

  async function getJson(url) {
    let response = await fetch(url);
    let data = await response.json()
    console.log(response)
    return data;
}

  function singUp(user,site){
    console.log(user.id)

    docRef = firebase.db.collection('usuario').doc(user.email)
    docRef.get().then((doc) => {
        if (doc.exists){
          console.log("User Already exists");
          if (site === "facebook"){
            docRef.update({
              nombre : JSON.stringify(user),
              profImgURL : user.picture.data.url,
              facebookId : user.id 
            })
          }  
        } else {
          var profileImgUrl,userId
          if (site === "facebook"){
            profileImgUrl = user.picture.data.url
            userId = user.id
          }else{
            profileImgUrl = user.photoUrl
            userId = ""
          }
          docRef.set({
            admin : false,
            banned : false,
            correo : user.email,
            dirección : "",
            fecha : null,
            nombre : user.name,
            telefono : "",
            profImgURL : profileImgUrl,
            facebookId : userId
          })
        }
    }).catch((error) => {
        console.log("Error getting document:", error);
    });

    
  }

  return (
    <View style = {styles.container}>
      <View style = {styles.header}>

        <TouchableOpacity style={[{marginRight:350,marginTop:10, height:20, width:20}]} onPress = {()=>{navigation.navigate('Principal')}}>
            <Icon
                name='arrow-left'
                type = 'font-awesome'
                size={20}
                color='#3D3D3D'
              />
        </TouchableOpacity>

        <Image
          source={require('../../../assets/HOLogo.jpg')}
          style={{marginTop:30, marginBottom:30,width: 350, height: 350 }}
        />

        <Text style = {[styles.title]}>Inicio de sesión</Text>


      </View>

      <View style = {styles.footer}>

          {/*<Text style = {styles.text}>Correo Electrónico</Text>
        
          <Input
            rightIcon={
              <Icon
                name='envelope-open'
                type = 'font-awesome'
                size={20}
                color='#3D3D3D'
              />
            }
          />
        
          <Text style = {styles.text}>Contraseña</Text>

          <Input
            rightIcon={
              <Icon
                name='lock'
                type = 'font-awesome'
                size={20}
                color='#3D3D3D'
              />
            }

            secureTextEntry={true} 
          /> */}
          <View style = {{marginTop:25}}>
            <Button 
              title = "Iniciar sesión con Google      "
              titleStyle ={[styles.text,{color : "white"}]}
              icon={<Icon name='google' type = 'font-awesome' size={20} color='white'/>}
              iconRight
              iconContainerStyle={{marginRight:30}}
              buttonStyle={{backgroundColor : "#E30447", marginLeft:30, marginRight:30, height:50}}
              onPress = {() => signInGoogle()}
            /> 
            
            <Button 
              title = "Iniciar sesión con Facebook   "
              titleStyle ={[styles.text,{color : "white"}]}
              icon={<Icon name='facebook-official' type = 'font-awesome' size={20} color='white'/>}
              iconRight
              iconContainerStyle={{marginRight:30}}
              buttonStyle={{backgroundColor : "#0061FF", marginLeft:30, marginRight:30, height:50, marginTop:30}}
              onPress = {()=> signInFacebook()}
            /> 

            <TouchableOpacity style={[{marginTop:70,height:20,marginRight:50,marginLeft:50}]} onPress = {()=>navigation.navigate('UserProfile')}>
              <Text style={[{textAlign: 'center', color:'#8C2771',fontSize:18}]}> Acerca de nuestra fundación </Text>
            </TouchableOpacity>

          </View>
     
          {/*<TouchableOpacity style={[{marginTop:70,height:20,marginRight:70,marginLeft:70}]} onPress = {()=>navigation.navigate('RestorePass')}>
            <Text style={[{textAlign: 'center', color:'#8C2771',fontSize:16}]}> ¿Olvidaste tu contraseña? </Text>
        </TouchableOpacity>*/}

      </View>
    </View>
  )
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#CCE6E7'
  },
  header: {
      flex: 2,
      justifyContent: 'center',
      alignItems: 'center'
  },
  footer: {
      flex: 1,
      backgroundColor: '#fff',
      borderTopLeftRadius: 30,
      borderTopRightRadius: 30,
      paddingVertical: 50,
      paddingHorizontal: 30
  },
  title: {
      textAlign: 'center',
      color: 'black',
      fontSize: 30,
      fontWeight: 'bold'
  },
  text: {
      color: '#3D3D3D',
      fontSize: 18 ,
      fontWeight: 'bold'
  },
  button: {
    backgroundColor:"red"
  } 
});