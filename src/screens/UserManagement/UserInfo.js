import React, {useState} from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Input} from 'react-native-elements';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Switch
} from 'react-native'

import firebase from '../../services/firebase';

export default function UserInfo ({navigation}){

  const [date, setDate] = useState(new Date());
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled(previousState => !previousState);

  function dateFieldText(){

    var todayDate = new Date()

    if (date.getFullYear() == todayDate.getFullYear()){
        return ''
    }else{
        return date.getDate() + '-' + (date.getMonth()+1) + '-' + date.getFullYear()
    }

  }

  return (
    <View style = {styles.container}>
       <View style = {styles.header}>

        <TouchableOpacity style={[{marginRight:350,marginTop:20, height:20, width:20}]} onPress = {()=>{navigation.navigate('Login')}}>
            <Icon
                name='arrow-left'
                type = 'font-awesome'
                size={20}
                color='#3D3D3D'
              />
        </TouchableOpacity>

      
        <Image
        source={{uri:"https://i1.wp.com/researchictafrica.net/wp/wp-content/uploads/2016/10/default-profile-pic.jpg?ssl=1"}}
        style={{ marginBottom:20,width: 200, height: 200 , borderRadius:200}}
        />
       

        <Text style = {[styles.title]}>Nombre Apellidos</Text>

      </View>

      <View style = {styles.footer}>
             
        <Input
        placeholder = 'Correo Electrónico'
        rightIcon={
            <Icon
            name='envelope-open'
            type = 'font-awesome'
            size={20}
            color='#3D3D3D'
            />}
        editable = {false}
        />

        <Input
        placeholder = 'Teléfono'
        rightIcon={
            <Icon
            name='phone'
            type = 'font-awesome'
            size={20}
            color='#3D3D3D'
            />}
        editable = {false}
        />

        <Input
        placeholder = 'Dirección'
        rightIcon={
            <Icon
            name='map-o'
            type = 'font-awesome'
            size={20}
            color='#3D3D3D'
            />}
        editable = {false} 
        />


        <Text style = {[styles.text,{marginLeft:10}]}>Fecha de nacimiento:</Text>
        <Input
            placeholder = {dateFieldText()}
            placeholderTextColor = "black"
            rightIcon={
            <Icon
                name='calendar'
                type = 'font-awesome'
                size={20}
                color='#3D3D3D'
            />}
            pointerEvents="none"
            editable = {false}
        />

            
        <View style = {{marginTop:80}}>
            <View style = {{flexDirection:'row'}}>
                <Text style = {[styles.text,{color:"#23B068"}]}>Usuario administrador</Text>
                <Switch
                    style = {{marginLeft:156}}
                    trackColor={{ false: "#767577", true: "#CCE6E7" }}
                    thumbColor={isEnabled ? "#522A32" : "#f4f3f4"}
                    onValueChange={toggleSwitch}
                    value={isEnabled}
                />
            </View>

            <View style = {{flexDirection:'row',marginTop:30}}>
                    <Text style = {[styles.text,{color:'red'}]}>Bloquear Usuario</Text>
                    <Switch
                        style = {{marginLeft:190}}
                        trackColor={{ false: "#767577", true: "#CCE6E7" }}
                        thumbColor={isEnabled ? "#522A32" : "#f4f3f4"}
                        onValueChange={toggleSwitch}
                        value={isEnabled}
                    />      
            </View>
        </View>

        
          {/*<TouchableOpacity style={styles.button}>
            <Text style={[styles.text,{textAlign: 'center', color:'white'}]}> Registrarse </Text>
          </TouchableOpacity>*/}

      </View>
    </View>
  )
}


const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#CCE6E7'
  },
  header: {
      flex: 0.7,
      justifyContent: 'center',
      alignItems: 'center'
  },
  footer: {
      flex: 1,
      backgroundColor: '#fff',
      borderTopLeftRadius: 30,
      borderTopRightRadius: 30,
      paddingVertical: 50,
      paddingHorizontal: 30
  },
  title: {
      marginTop:0,
      textAlign: 'center',
      color: 'black',
      fontSize: 30,
      fontWeight: 'bold'
  },
  text: {
      color: '#3D3D3D',
      fontSize: 18 ,
      fontWeight: 'bold'
  },
  button: {
    marginRight:70,
    marginLeft:70,
    marginTop:20,
    paddingTop:15,
    paddingBottom:15,
    backgroundColor:'#A68358',
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#fff',
  } });