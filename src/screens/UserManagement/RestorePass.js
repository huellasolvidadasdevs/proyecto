import React, {useState} from 'react';
import {Input} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

import firebase from '../../services/firebase';

import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
} from 'react-native'

export default function RestorePass ({navigation}) {

  return (
    <View style = {styles.container}>
      <View style = {styles.header}>
        
        <TouchableOpacity style={[{marginRight:350,marginTop:15,flex:0.1, height:20, width:20}]} onPress = {()=>navigation.navigate('Login')}>
            <Icon
                name='arrow-left'
                type = 'font-awesome'
                size={20}
                color='#3D3D3D'
              />
        </TouchableOpacity>  
      
        <Text style = {[styles.title,{flex:0.7,marginTop:140}]}>Recuperar Contraseña</Text>
      
      </View>

      <View style = {styles.footer}>
             
          <Input 
            placeholder = 'Correo Electrónico'
            /*rightIcon={
              <Icon
                name='envelope-open'
                type = 'font-awesome'
                size={20}
                color='#3D3D3D'
              />}*/
          />
   
          <TouchableOpacity style={styles.button}>
            <Text style={[styles.text,{textAlign: 'center', color:'white'}]}> Verificar </Text>
          </TouchableOpacity>

      </View>
    </View>
  )
}

//export default RestorePass;

const {height} = Dimensions.get("screen");
const height_logo = height * 0.01;

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#CCE6E7'
  },
  header: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
  },
  footer: {
      flex: 1,
      backgroundColor: '#fff',
      borderTopLeftRadius: 30,
      borderTopRightRadius: 30,
      paddingVertical: 50,
      paddingHorizontal: 30
  },
  logo: {
      width: height_logo,
      height: height_logo
  },
  title: {
      textAlign: 'center',
      color: 'black',
      fontSize: 30,
      fontWeight: 'bold'
  },
  text: {
      color: '#3D3D3D',
      fontSize: 18 ,
      fontWeight: 'bold'
  },
  button: {
    marginRight:70,
    marginLeft:70,
    marginTop:20,
    paddingTop:15,
    paddingBottom:15,
    backgroundColor:'#A68358',
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#fff',
  } });