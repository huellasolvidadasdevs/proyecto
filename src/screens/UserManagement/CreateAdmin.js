import React, {useState} from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Input} from 'react-native-elements';
import DateTimePicker from '@react-native-community/datetimepicker'; 
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
} from 'react-native'

import firebase from '../../services/firebase';

const SingUp = ({navigation}) => {

  const [date, setDate] = useState(new Date());
  const [show, setShow] = useState(false);

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(false);
    setDate(currentDate);
  };

  /*const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };*/

  const showDatepicker = () => {
    setShow(true);
    //showMode('date');
  };


  return (
    <View style = {styles.container}>
      <View style = {styles.header}>
        
        <TouchableOpacity style={[{marginRight:350,marginTop:20, height:20, width:20}]} onPress = {() => {return}}>
            <Icon
                name='arrow-left'
                type = 'font-awesome'
                size={20}
                color='#3D3D3D'
              />
        </TouchableOpacity>

        <Text style = {[styles.title]}>Crear Administrador</Text>

      </View>

      <View style = {styles.footer}>
             
          <Input
            placeholder = 'Nombre'
          />

          <Input
            placeholder = 'Apellidos'
          />

          <Input
            placeholder = 'Correo Electrónico'
            rightIcon={
              <Icon
                name='envelope-open'
                type = 'font-awesome'
                size={20}
                color='#3D3D3D'
              />}
          />
        
          <Input
              placeholder = 'Teléfono'
              rightIcon={
                <Icon
                  name='lock'
                  type = 'font-awesome'
                  size={20}
                  color='#3D3D3D'
                />} 
          />

          <Input
              placeholder = 'Contraseña'
              rightIcon={
                <Icon
                  name='lock'
                  type = 'font-awesome'
                  size={20}
                  color='#3D3D3D'
                />}
             secureTextEntry={true} 
          />

          <Input
              placeholder = 'Confirmar contraseña'
              rightIcon={
                <Icon
                  name='lock'
                  type = 'font-awesome'
                  size={20}
                  color='#3D3D3D'
                />}
             secureTextEntry={true} 
          />

          <TouchableOpacity onPress={showDatepicker}>
            <Input
              placeholder = {date.getDate() + '-' + (date.getMonth()+1) + '-' + date.getFullYear()}
              placeholderTextColor = "black"
              rightIcon={
                <Icon
                  name='calendar'
                  type = 'font-awesome'
                  size={20}
                  color='#3D3D3D'
                />}
              pointerEvents="none"
              editable = {false}
            />
          </TouchableOpacity>
               
          {show && (
            <DateTimePicker
              testID="dateTimePicker"
              value={date}
              mode="date"
              is24Hour={true}
              display="spinner"
              maximumDate={new Date()}
              onChange={onChange}
            />
            )}
   

          <TouchableOpacity style={styles.button}>
            <Text style={[styles.text,{textAlign: 'center', color:'white'}]}> Registrarse </Text>
          </TouchableOpacity>

      </View>
    </View>
  )
}

export default SingUp;

const {height} = Dimensions.get("screen");
const height_logo = height * 0.01;

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#CCE6E7'
  },
  header: {
      flex: 0.2,
      justifyContent: 'center',
      alignItems: 'center'
  },
  footer: {
      flex: 1,
      backgroundColor: '#fff',
      borderTopLeftRadius: 30,
      borderTopRightRadius: 30,
      paddingVertical: 50,
      paddingHorizontal: 30
  },
  logo: {
      width: height_logo,
      height: height_logo
  },
  title: {
      marginTop:0,
      textAlign: 'center',
      color: 'black',
      fontSize: 30,
      fontWeight: 'bold'
  },
  text: {
      color: '#3D3D3D',
      fontSize: 18 ,
      fontWeight: 'bold'
  },
  button: {
    marginRight:70,
    marginLeft:70,
    marginTop:20,
    paddingTop:15,
    paddingBottom:15,
    backgroundColor:'#A68358',
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#fff',
  } });