import React, {useState} from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Input} from 'react-native-elements';
import DateTimePicker from '@react-native-community/datetimepicker';
import * as Google from 'expo-google-app-auth';
import firebase from '../../services/firebase';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
 
} from 'react-native'
import Login from './Login';

export default function UserProfile ({route,navigation}){

  const {id,site} = route.params;

  const [date, setDate] = useState(new Date());
  const [show, setShow] = useState(false);
  const userDocRef = getDocRef();
  
  function getDocRef(){
    var docRef;
    if (site === "facebook"){
      docRef = firebase.db.collection('usuario').where('facebookId','==',id)
    }else{
      docRef = firebase.db.collection('usuario').doc(id)
    }
    console.log(docRef)
    return docRef;
  }

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(false);
    setDate(currentDate);
  };

  const showDatepicker = () => {
    setShow(true);
  };

  function dateFieldText(){

    var todayDate = new Date()

    if (date.getFullYear() == todayDate.getFullYear()){
        return ''
    }else{
        return date.getDate() + '-' + (date.getMonth()+1) + '-' + date.getFullYear()
    }

  }

  const singOutAsync =  async () => {
      await Google.logOutAsync({ accessToken, ...config });
      navigation.navigate(Login)
  };


  return (
   <View style = {styles.container}>
       <View style = {styles.header}>

        <TouchableOpacity style={[{marginRight:350,marginTop:20, height:20, width:20}]} onPress = {()=>{navigation.navigate('Login')}}>
            <Icon
                name='arrow-left'
                type = 'font-awesome'
                size={20}
                color='#3D3D3D'
              />
        </TouchableOpacity>

      
        {/*<Image
        source={{uri: userData.profImgUrl}}
        style={{ marginBottom:20,width: 200, height: 200 , borderRadius:200}}
        />*/}
       

        {/*<Text style = {[styles.title]}>{userDocRef.get('nombre')}</Text>*/}

      </View>

      {/*<View style = {styles.footer}>
             
        <Input
        placeholder = {userDocRef.get(String(userDocRef.get('profImgURL')))}
        placeholderTextColor = "black"
        rightIcon={
            <Icon
            name='envelope-open'
            type = 'font-awesome'
            size={20}
            color='#3D3D3D'
            />}
        editable = {false}
        />

        <Input
        placeholder = 'Teléfono'
        rightIcon={
            <Icon
            name='phone'
            type = 'font-awesome'
            size={20}
            color='#3D3D3D'
            />} 
        />

        <Input
        placeholder = 'Dirección'
        rightIcon={
            <Icon
            name='map-o'
            type = 'font-awesome'
            size={20}
            color='#3D3D3D'
            />} 
        />

        <Text style = {[styles.text,{marginLeft:10}]}>Fecha de nacimiento:</Text>
        <TouchableOpacity onPress={showDatepicker}>
        <Input
            placeholder = {dateFieldText()}
            placeholderTextColor = "black"
            rightIcon={
            <Icon
                name='calendar'
                type = 'font-awesome'
                size={20}
                color='#3D3D3D'
            />}
            pointerEvents="none"
            editable = {false}
        />
        </TouchableOpacity>
            
        {show && (
        <DateTimePicker
            testID="dateTimePicker"
            value={date}
            mode="date"
            is24Hour={true}
            display="spinner"
            maximumDate={new Date()}
            onChange={onChange}
        />
        )}

        <TouchableOpacity style={[{marginTop:70,height:20,marginRight:50,marginLeft:50}]} onPress = {()=>singOutAsync()}>    
              <Text style={[{textAlign: 'center', color:'#8C2771',fontSize:18}]}> Cerrar Sesión </Text>
        </TouchableOpacity>
   

          {/*<TouchableOpacity style={styles.button}>
            <Text style={[styles.text,{textAlign: 'center', color:'white'}]}> Registrarse </Text>
          </TouchableOpacity>

      </View>*/}
        </View>
  )
}


const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#CCE6E7'
  },
  header: {
      flex: 0.7,
      justifyContent: 'center',
      alignItems: 'center'
  },
  footer: {
      flex: 1,
      backgroundColor: '#fff',
      borderTopLeftRadius: 30,
      borderTopRightRadius: 30,
      paddingVertical: 50,
      paddingHorizontal: 30
  },
  title: {
      marginTop:0,
      textAlign: 'center',
      color: 'black',
      fontSize: 30,
      fontWeight: 'bold'
  },
  text: {
      color: '#3D3D3D',
      fontSize: 18 ,
      fontWeight: 'bold'
  },
  button: {
    marginRight:70,
    marginLeft:70,
    marginTop:20,
    paddingTop:15,
    paddingBottom:15,
    backgroundColor:'#A68358',
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#fff',
  } });