import { SafeAreaProvider } from 'react-native-safe-area-context';
import {styles} from './StylesPrincipal.js'
import React, {Component} from 'react'
import { Card, Icon} from 'react-native-elements';
import {  Text, View, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

const list = [{name: "María Eugenia", date:"12", sex:true},
{name: "Stef", date:"3", sex:false}, {name: "Kira", date:"5", sex:true}, 
{name: "Susy", date:"Zasha", sex:false},{name: "a", date:"5", sex:false},
{name: "a", date:"5", sex:true}]

export default function PrincipalScreen({navigation}) {

    return (
      <SafeAreaProvider>
        <View>
        <ScrollView style={styles.scrollViewPrincipal}>
        {/* <Search></Search> */}
        
        {list.map(element=>(
          <TouchableOpacity  onPress={()=>
            navigation.navigate('Menu')}>
          <CardDog name={element.name} date={element.date} sex={element.sex}></CardDog>
          </TouchableOpacity>
          ))
          }
        </ScrollView>
        
      </View>
      </SafeAreaProvider>
    );
    
  }
  function CardDog (props) {
    return(
      
      <Card >
        <View style={styles.viewCardHeader} >
          <View style={styles.viewTextHeader}><Text style={styles.dogTitle}>{props.name}</Text>{props.sex ? 
            <Icon name="venus" size={15} type="font-awesome" color="pink" style={styles.iconSex}></Icon>:
            <Icon name="mars" size={15} type="font-awesome" color="darkblue" style={styles.iconSex}></Icon>}
            </View>
          <View style={styles.viewIcons}>
          <Icon name="trash" type="font-awesome" color="red" styles={styles.iconDelete}></Icon></View>
        
          </View>
  
        <Card.Divider></Card.Divider>
  
        <View>
          <Text> {props.date}</Text>
          <Text> {props.sex}</Text>
        </View>
      </Card>
    )
  }

