import {  StyleSheet} from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import React from 'react';
import { Text, TouchableOpacity, TouchableHighlight } from 'react-native';
import { Button, Icon} from 'react-native-elements';
import { View } from 'react-native';
import { ThemeProvider } from '@react-navigation/native';
export default function MenuAdmin(){
    return(
    <SafeAreaProvider>
        <BarBottom></BarBottom>
    </SafeAreaProvider>
    );
}
class BarBottom extends React.Component{
    constructor(props){
        super(props)
        this.state ={
            currentColor1: '#CFB38C',
            currentColor2: '#CFB38C',
            currentColor3: '#CFB38C',
            currentColor4: '#CFB38C'
        }
    }
    changeColor(number){
        if (number == 1){
            this.setState({currentColor1: "#A67F5D", currentColor2:'#CFB38C',
            currentColor3:'#CFB38C', currentColor4:'#CFB38C'})
        }else if(number==2){
            this.setState({currentColor1: '#CFB38C', currentColor2:"#A67F5D",
            currentColor3:'#CFB38C', currentColor4:'#CFB38C'})
        }
        else if(number==3){
            this.setState({currentColor1: '#CFB38C', currentColor2:'#CFB38C',
            currentColor3:"#A67F5D", currentColor4:'#CFB38C'})
        }
    }
    render(){
    
    return(
        <SafeAreaProvider>
            <View style={stylesDogs.container}>
                <View style={stylesDogs.containerIcon}>
                    <TouchableOpacity onPress={()=>this.changeColor(1) }>
                    <Icon name="user-o" type="font-awesome" color= {this.state.currentColor1} ></Icon>  
                    <Text style={[stylesDogs.textBarBottom], {color: this.state.currentColor1}} >Perfil</Text>
                    </TouchableOpacity>
               </View>
               <View style={stylesDogs.containerIcon}>
                    <TouchableOpacity onPress={()=>this.changeColor(2) }>
                    <Icon name="picture-o" type="font-awesome" color= {this.state.currentColor2}></Icon>  
                    <Text style={[stylesDogs.textBarBottom], {color: this.state.currentColor2}} onPress={()=>
                                navigation.navigate('Menu')}>Historias</Text>
                    </TouchableOpacity>
               </View>
               <View style={stylesDogs.containerIcon}> 
                    <TouchableOpacity onPress={()=>this.changeColor(3) }>
                    <Icon name="heartbeat" type="font-awesome" color= {this.state.currentColor3}></Icon>  
                    <Text style={[stylesDogs.textBarBottom], {color: this.state.currentColor3}}>Medico</Text>
                    </TouchableOpacity>
               </View>
            </View>
        </SafeAreaProvider>
    ); }
}

const stylesDogs = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        borderTopWidth: 1,
        borderTopColor: "#CFB38C",
        position: 'absolute',
        alignItems:'center',
        bottom: 0,
    },
    button: {
        marginBottom: 5,
        alignSelf: 'stretch',
        fontSize: 30,
    },
    containerIcon:{
        padding: 10,
        width: '33%',
        alignItems: 'center',
        flexDirection: 'column',
        marginBottom: 5,
        marginTop: 10,
        
    },
    textBarBottom:{
        fontSize: 12,
    }
    
})

    




