import {  StyleSheet} from 'react-native';
export const styles = StyleSheet.create({
    container: {
      flex: 1,
      top: 0,
    },
    navBar: {
      height: '40px',
    }, 
    header: {
      padding: 0,
    },
    dogTitle: {
      maxWidth: 150,
      fontSize: 20,
      alignSelf: 'flex-start',
    },
    viewCardHeader:{
      flexDirection: 'row',
      alignItems: 'center',
      
    },
    scrollViewPrincipal:{
      marginBottom: 20
    },
    viewIcons: {
      flex: 1,
  
    },
    iconDelete: {
      flex: 3,
    },
    viewTextHeader:{
      flex: 3,
      flexDirection:'row',
    },
    iconSex:{
      marginStart: 8,
      alignSelf: 'center',
    },
    buttonAdd:{
      marginEnd: 15
    }
  });