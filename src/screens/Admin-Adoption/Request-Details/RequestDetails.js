import React, { useEffect, useState } from "react";
import {
    Text,
    View,
    Alert,
    Button,
    StyleSheet,
    ScrollView,
    ActivityIndicator,
    TouchableOpacity,
} from "react-native";
import { Card } from 'react-native-elements';
import firebase from "../../../services/firebase";

const RequestDetails = (props) => {
    const initialStateRquest = {
        id: "",
        userid: "",
        dogid: "",
        state: "",
        info: "",
    };

    const initialStateUser = {
        id: "",
        email: "",
        name: "",
        phone: "",
    };

    const initialStateDog = {
        id: "",
        name: "",
        state: "",
    };

    const [request, setRequest] = useState(initialStateRquest);
    const [user, setUser] = useState(initialStateUser);
    const [dog, setDog] = useState(initialStateDog);
    const [loading, setLoading] = useState(true);

    const setData = async (requestId,requestUserId,requestDogID,requestState,requestInfo) => {
        setRequest({ 
            requestId,
            requestUserId,
            requestDogID,
            requestState,
            requestInfo
        });

        const dbRef1 = firebase.db.collection("users").doc(requestUserId);
        const doc1 = await dbRef1.get();
        const user = doc1.data();

        const dbRef2 = firebase.db.collection("dogs").doc(requestDogID);
        const doc2 = await dbRef2.get();
        const dog = doc2.data();
        
        setUser({ ...user, id: doc1.id });
        setDog({ ...dog, id: doc2.id });

        setLoading(false)
    };

    useEffect(() => {
        setData(
            props.route.params.requestId,
            props.route.params.requestUserId,
            props.route.params.requestDogID,
            props.route.params.requestState,
            props.route.params.requestInfo,
        )
    }, []);

    if (loading) {
        return (
            <View style={styles.loader}>
              <ActivityIndicator size="large" color="#9E9E9E" />
            </View>
        );
    }

    const onProcesRequest = async (answer) => {

        if (answer == "Aceptada"){
            firebase.db.collection("requests").onSnapshot((querySnapshot) => {
                querySnapshot.docs.forEach((doc) => {
                    if(doc.dogID == request.dogid && doc.id != request.requestId){
                        const auxRef = firebase.db.collection("requests").doc(doc.id)
                        auxRef.update({
                            state: "Rechazada",
                        });
                    }
                });
            });
        }

        const requestRef = firebase.db.collection("requests").doc(request.requestId);
        await requestRef.update({
            state: answer,
        });
        
        props.navigation.navigate("Requestlist");
    };

    const renderCancel = () => {
        if (request.requestState == "Pendiente") {
            return (
                <View style={styles.btnContainer}>
                    <View style={styles.btnRechazar}>
                        <Button
                            title="Rechazar"
                            color="#DC143C"
                            onPress = {() => onProcesRequest("Rechazada")}
                        />
                    </View>
                    <View style={styles.btnAceptar}>
                        <Button 
                            title="Aceptar" 
                            color="#19AC52" 
                            onPress = {() => onProcesRequest("Aceptada")}
                        />
                    </View>
                </View>
            );
        } else {
            return null;
        }
    }

    return (
        <View style={styles.container}>
            <Card style={styles.infoContainer}>
                <View>
                    <TouchableOpacity style={styles.btnUser}>
                        <Text style={styles.inputBtn}>
                            Usuario: {user.name}
                        </Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity style={styles.btnDog}>
                        <Text style={styles.inputBtn}>
                            Perro: {dog.name}
                        </Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <Text style={styles.inputGroup}>
                        Estado: {request.requestState}
                    </Text>
                </View>
                <View>
                    <Text style={styles.inputGroup}>
                    {request.requestInfo}
                    </Text>
                </View>
            </Card>
            {renderCancel()}  
        </View>
        
    );
}
const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        position: "absolute",
        flexDirection: 'column',
        backgroundColor: 'lightpink',
    },
    infoContainer: {
        height: '87%',
        borderRadius: 8,
    },
    loader: {
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        position: "absolute",
        alignItems: "center",
        justifyContent: "center",
    },
    inputGroup: {
        flex: 1,
        padding: 0,
        marginBottom: 15,
    },
    inputBtn: {
        flex: 1,
        padding: 0,
        margin: 10,
    },
    btnContainer: {
        marginBottom: 15,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 0,
        flex: 1,
    },
    btnAceptar: {
        width: '50%',
        alignItems: 'center',
    },
    btnRechazar: {
        width: '50%',
        alignItems: 'center',
    },
    btnUser: {
        justifyContent: 'center',
        borderRadius: 8,
        borderWidth: 1,
        marginBottom: 15,
    },
    btnDog: {
        justifyContent: 'center',
        borderRadius: 8,
        borderWidth: 1,
        marginBottom: 15,
    },
});

export default RequestDetails
