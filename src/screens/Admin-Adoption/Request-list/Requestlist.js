import React, { useState, useEffect } from "react";
import { Button, StyleSheet } from "react-native";
import { ListItem, Avatar } from "react-native-elements";
import { ScrollView } from "react-native-gesture-handler";

import firebase from "../../../services/firebase";

const Requestlist = (props) => {
    const [requests, setRequest] = useState([]);

    useEffect(() => {
        firebase.db.collection("requests").onSnapshot((querySnapshot) => {
          const requests = [];
          querySnapshot.docs.forEach((doc) => {
            const { info, userID, dogID, state } = doc.data();
            requests.push({
              id: doc.id,
              info,
              userID,
              dogID,
              state
            });
          });
          setRequest(requests);
        });
    }, []);

    return (
        <ScrollView>
      {requests.map((request) => {
        return (
          <ListItem
            key={request.id}
            bottomDivider
            onPress={() => {
              props.navigation.navigate("RequestDetails", {
                requestId: request.id,
                requestUserId: request.userID,
                requestDogID: request.dogID,
                requestState: request.state,
                requestInfo: request.info,
              });
            }}
            >
            <ListItem.Chevron/>
            <ListItem.Content>
              <ListItem.Title style={styles.title}>Solicitud de Adopción de {request.username}</ListItem.Title>
              <ListItem.Subtitle style={styles.item}>{request.state}</ListItem.Subtitle>
            </ListItem.Content>
          </ListItem>
        );
      })}
    </ScrollView>
    )
}

export default Requestlist

const styles = StyleSheet.create({
    title: {
        fontSize: 15,
        fontFamily: "sans-serif-light",
      },
    item: {
      fontSize: 12,
      fontFamily: "sans-serif-light",
    }
});